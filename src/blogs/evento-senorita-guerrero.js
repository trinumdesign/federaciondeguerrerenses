export default {
  "timestamp": "Julio, 2020",
  "title": "Aprendiendo sobre nuestra cultura y tradiciones",
  "socials": [],
  "sections": [
    {
      type: "txt",
      content: "● Las participantes en el evento Señorita Guerrero en Chicago 2019, fueron invitadas a nuestro estado para conocer diferentes lugares y algunas de nuestras tradiciones."
    },
    {
      type: "txt",
      content: "● En el mes de julio del año 2019, gracias al aporte de la Secretaría de Asuntos Migrantes e Internacionales del Estado de Guerrero, la \“Reina Guerrero en Chicago 2019-2020\” y las princesas del evento de Elección y Coronación de ese año, fueron premiadas con un inolvidable viaje turístico a Guerrero, esto como parte de un regalo que se les había prometido por el simple hecho de participar en aquel certamen de febrero de 2019. Las cuatro representantes de la comunidad guerrerense radicada en Chicago: Lizbeth Ramírez, Reina en aquel entonces y las princesas: Liliana Urióstegui, Jasmine Salgado y Jackie Sánchez, conocieron los principales municipios del Estado de Guerrero, lugar de origen de algunas de ellas o al menos, de uno de sus padres."
    },
    {
      type: "header",
      content: "El principal objetivo"
    },
    {
      type: "txt",
      content: "\"Que a su regreso contaran la maravillosa experiencia a niños y a los más jovencitos, no solo para difundir sino además fomentar el amor por Guerrero y despertar la curiosidad y el interés de los hijos y nietos de migrantes que nacieron en Estados Unidos, por conocer la tierra de sus progenitores, ya sean padres o abuelos, enseñando de esta manera el valor y el respeto por las tradiciones culturales y costumbres guerrerenses\”."
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8306_pjblrq.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8299_d5oenz.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8303_jds5xw.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8300_vzesdm.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8301_tfa2ao.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8298_beyfzw.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8302_hjd4ya.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8291_pazv8x.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8297_eembh5.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8293_ddgghw.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8295_ihuryw.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8294_rxmgtq.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8292_iiaxyb.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8296_pl7hgu.jpg"
    }
  ]
}
