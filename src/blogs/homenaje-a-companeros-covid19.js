export default {
  "timestamp": "Febrero, 2021",
  "title": "Homenaje a compañeros y amigos que ya no están entre nosotros",
  "socials": [],
  "sections": [
    {
      type: "txt",
      content: "● En el último año, no solo la pandemia de COVID-19 sino otras enfermedades o por causas naturales, algunos de nuestros compañeros de la organización, activistas comunitarios colegas y grandes colaboradores y amigos de nuestra causa proinmigrante han fallecido, dejando un enorme vacío entre nosotros."
    },
    {
      type: "txt",
      content: "● En este nuestro sitio virtual, queremos rendir un homenaje a la memoria no solo del periodista y amigo Miguel Ángel Arrieta Martínez, y de los compañeros activistas Nestora Rodríguez y Eduardo Díaz; sino de muchos otros que hoy desafortunadamente ya no están entre nosotros. Son ellos:"
    },
    {
      type: "txtxlst",
      content: "Audifas Delgado Arroyo,Juventino Arroyo Delgado,Porfirio Delgado,Roberto Moreno,Metodio Moreno,Josefa Arroyo,Alberto Arroyo \"Don Amancio\",Tomasa Delgado Arroyo,Felipe Delgado,Toribio Delgado,Francisco Delgado Delgado,Rosendo Arroyo Delgado,Alicia Arias,Roberta Boyas de Pacheco,Ignacia Arroyo Boyas,Claudia Delgado Delgado,Agustín Delgado Arroyo,María Luisa Boyas,Demetrio Bautista,Contorbey Herrera,Rumencio Arroyo Castrejón,Naty Robles,Paulino Álvarez,Antolina Díaz,Freddie Marchán,Benito Hernández Lara,Miguel Ángel Arroyo,Pedro Marchán,Damaceno Villalobos Fernández,Agustín Román,Timoteo Vázquez,Agustina Mendoza,Antonio Bustamante,Poli Olayo,Elizondo Villalobos,Félix Bustamante,Fidel Brito,Irais Martínez,María Concepción Román,Rabid Gonzaga,Uriel Brito."
    },
    {
      type: "txtxbld",
      content: "¡PAZ EN LA TUMBA DE TODOS ESTOS COMPAÑEROS, CONOCIDOS Y AMIGOS!"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/v1613940220/federacion-de-guerrerenses/portada_guerrero/16a49c2b-cba7-4d58-baa3-b51f14750658_mcr3kx.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/v1613940337/federacion-de-guerrerenses/portada_guerrero/a46a90fb-70c2-4a6c-bac7-32a63da790ae_pdisyr.jpg"
    },
    {
      type: "uriximg",
      content: "https://res.cloudinary.com/trinum-daniel/image/upload/w_300/v1613940221/federacion-de-guerrerenses/portada_guerrero/bd7fe010-3ca7-40fc-a735-548e7a6afea2_tdm5t0.jpg"
    }
  ]
}
