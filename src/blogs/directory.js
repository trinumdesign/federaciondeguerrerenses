export default {
  "07_01_2019": [
    "evento-senorita-guerrero*Aprendiendo sobre nuestra cultura y tradiciones"
  ],
  "06_01_2020": [
    "entrega-de-despensas-covid19*Entrega de despensas"
  ],
  "02_21_2021" : [
    "mensaje-de-presidente*Mensaje de el presidente",
    "homenaje-a-companeros-covid19*Homenaje a compañeros y amigos",
    "orgullo-de-guerrero*ISAAC HELGUERA: Un artista ¡Orgullo de Guerrero!"
  ]
}
