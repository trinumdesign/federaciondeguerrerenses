export default {
  "timestamp": "Junio, 2020",
  "title": "Entrega de despensas para los afectados por la pandemia de COVID-19",
  "socials": [],
  "sections": [
    {
      type: "txt",
      content: "● Nuestras familias afectadas en pandemia no han sido olvidadas. Hemos llevado a cabo entrega de despensas para quienes han sufrido los efectos del COVID-19."
    },
    {
      "type": "uriximg",
      "content": "https://res.cloudinary.com/trinum-daniel/image/upload/w_500/federacion-de-guerrerenses/despensas_junio/IMG_8123_1_b17hdb.jpg"
    },
    {
      "type": "uriximg",
      "content": "https://res.cloudinary.com/trinum-daniel/image/upload/w_500/federacion-de-guerrerenses/despensas_junio/IMG_8110_1_ouoebk.jpg"
    },
    {
      "type": "uriximg",
      "content": "https://res.cloudinary.com/trinum-daniel/image/upload/w_500/federacion-de-guerrerenses/despensas_junio/IMG_8111_1_am8bs3.jpg"
    },
    {
      "type": "uriximg",
      "content": "https://res.cloudinary.com/trinum-daniel/image/upload/w_500/federacion-de-guerrerenses/despensas_junio/IMG_8114_1_eabnwu.jpg"
    },
    {
      "type": "uriximg",
      "content": "https://res.cloudinary.com/trinum-daniel/image/upload/w_500/federacion-de-guerrerenses/despensas_junio/IMG_8112_1_nihmqv.jpg"
    }
  ]
}
