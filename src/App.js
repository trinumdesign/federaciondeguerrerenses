import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import { FiChevronUp } from 'react-icons/fi';

import Toolbar from './components/Toolbar';
import Landing from './components/Landing';
import Blogs from './components/Blogs';
import Blog from './components/Blog';
import Club from './components/Club';
import Gallery from './components/Gallery';
import Events from './components/Events';
import Footer from './components/Footer';

import './App.css'

class App extends React.Component {
  state = {
    showFab: false
  }

  constructor(props) {
    super(props);

    this.state = {
      showFab: false
    }

    setTimeout(() => {
      this.onRouteChange(props.location, '');
    }, 800);

    props.history.listen((data) => {
      setTimeout(() => {
        this.onRouteChange(data, props.history);
      }, 500);
    });
  }

  onRouteChange(data, history) {
    if (history.action === "REPLACE") return;
    let element;

    if (data.pathname.includes('/blog/')) {
      window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
      this.setState({
        showFab: false
      });
      return;
    }

    if (data.pathname.includes('/clubs/')) {
      window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
      this.setState({
        showFab: false
      });
      return;
    }

    switch(data.pathname) {
      case '/':
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        this.setState({
          showFab: false
        });
        break;
      case '/events':
        element = document.getElementById('events')
        this.setState({
          showFab: false
        });
        break;
      case '/blogs':
        element = document.getElementById('blogs')
        this.setState({
          showFab: false
        });
        break;
      case '/clubs':
        element = document.getElementById('clubs')
        this.setState({
          showFab: false
        });
        break;
      case '/contact':
        element = document.getElementById('footer')
        this.setState({
          showFab: false
        });
        break;
      default:
        this.setState({
          showFab: false,
          location: data
        });
        break;
    }

    if (!element) return;
    element.scrollIntoView({behavior: 'smooth'});
  }

  componentDidMount() {
    document.addEventListener('scroll', (e) => {
      this.setState({
        showFab: window.scrollY > 86
      })
    });
  }

  BackToTop() {
    document.getElementById('top').scrollIntoView({behavior: 'smooth'});
  }

  render() {
    return (
      <React.Fragment>
        <Toolbar />
        <Route path="/blog/:date/:identifier" component={Blog}></Route>
        <Route path="/clubs/:identifier" component={Club}></Route>
        <Route path={["/", "/events", "/blogs", "/clubs", "/contact"]} exact>
          <Landing />
          <Events/>
          <Blogs />
          <Gallery />
        </Route>
        <Footer />
        <div onClick={this.BackToTop} className={"fab flexible center popup primaryFill bckTTp" + (this.state.showFab ? ' show' : '')}>
          <FiChevronUp className="tertiary" />
        </div>
      </React.Fragment>
    )
  }
}

export default withRouter(App);
