import React from 'react';
import '../styles/Blogs.css'
import { NavLink } from 'react-router-dom';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';

import events from '../blogs/directory';

import moment from 'moment';
import 'moment/locale/es';
moment.locale('es');
function Blogs() {

  const [date, setDate] = React.useState(moment());
  const [calendar, setCalendar] = React.useState([]);

  React.useEffect(() => {
    let first = date.clone().startOf('month').day();
    let daysOfMonth = [];
    let daysOfLastMonth = [];
    let daysOfNextMonth = [];

    if (first !== 0) {
      let lastMonth = date.clone().startOf('month').subtract(1, 'd');
      for (let i = 0; i < first; i++) {
        daysOfLastMonth.push(lastMonth.format('MM/DD/YYYY'));
        lastMonth = lastMonth.subtract(1, 'd');
      }
    }
    for (let i = 0; i < date.clone().endOf('month').date(); i++) {
      daysOfMonth.push(date.clone().startOf('month').add(i, 'd').format('MM/DD/YYYY'));
    }
    let end = date.clone().endOf('month').day();
    if (end !== 6) {
      let nextMonth = date.clone().endOf('month').add(1, 'd');
      for (let i = 0; i < 6 - end; i++) {
        daysOfNextMonth.push(nextMonth.format('MM/DD/YYYY'));
        nextMonth = nextMonth.add(1, 'd');
      }
    }
    setCalendar([...daysOfLastMonth.reverse(), ...daysOfMonth, ...daysOfNextMonth])
  }, [date]);

  const GetClasses = (index, day) => {
    let classes = [];

    if (index < 7) {
      classes.push('tr');
    } else if (index >= calendar.length - 7) {
      classes.push('br');
    }

    if (!date.isSame(day, 'month')) {
      classes.push('past');
    }


    if (index % 7 == 0) {
      classes.push('l')
    } else if ((index + 1) % 7 == 0) {
      classes.push('r')
    }

    return classes.join(" ");
  }

  const GetDay = (index) => {
    if (index === 0) return 'DOM';
    if (index === 1) return 'LUN';
    if (index === 2) return 'MAR';
    if (index === 3) return 'MIE';
    if (index === 4) return 'JUE';
    if (index === 5) return 'VIE';
    if (index === 6) return 'SAB';
  }

  const FormatDay = (day) => {
    let formatted = moment(day);

    let lastOfLast = date.clone().startOf('month').subtract(1, 'd');
    let firstOfNext = date.clone().startOf('month').add(1, 'M');

    if (lastOfLast.isSame(day) || firstOfNext.isSame(day)) {
      return formatted.format('MMM').charAt(0).toUpperCase() + formatted.format('MMM').slice(1) + " " + formatted.format("DD")
    }

    return formatted.date();
  }

  const LastMonth = () => {
    let lastMonth = date.clone().subtract(1, 'M');
    setDate(lastMonth);
  }

  const NextMonth = () => {
    let nextMonth = date.clone().add(1, 'M');
    setDate(nextMonth);
  }

  const GetMonth = () => {
    return date.format('MMMM').charAt(0).toUpperCase() + date.format('MMMM').slice(1) + " " + date.format("YYYY")
  }

  const GetUri = (ev) => {
    return ev.split('*')[0]
  }

  const GetName = (ev) => {
    return ev.split('*')[1]
  }

  return (
    <>
      <div id="blogs" className="svnty-height flexible full-width center columns wrap bm2">
        <h4 className="flexible center full-width primary uppercase bm">Nuestro Calendario</h4>
        <h1 className="flexible center secondary text-center tm bm2"><FiChevronLeft onClick={() => LastMonth()} className="clickable primary"/>&nbsp;&nbsp;<span className="flexible center enlarge">{GetMonth()}</span>&nbsp;&nbsp;<FiChevronRight onClick={() => NextMonth()} className="clickable primary"/></h1>
        <div className="flexible center rows wrap full-width c-cont">
          {
            calendar.map((day, index) => {
              return (
                <div className={"calendar-day " + GetClasses(index, day)}>
                  {
                    index < 7 &&
                    <h5 className="flexible center full-width">{GetDay(index)}</h5>
                  }
                  <h4 className="flexible center full-width">{FormatDay(day)}</h4>
                  <br/>
                  {
                    events[day.replace(/\//g, "_")] &&
                    <>
                      {
                        events[day.replace(/\//g, "_")].map((entry) => {
                          return (
                            <NavLink to={`/blog/${day.replace(/\//g, "_")}/${GetUri(entry)}`} className="flexible full-width entry secondary">&#x25cf; {GetName(entry)}</NavLink>
                          )
                        })
                      }
                    </>
                  }
                </div>
              )
            })
          }
        </div>
      </div>
    </>
  )
}

export default Blogs;
