import React from 'react';
import { NavLink } from 'react-router-dom';

import '../styles/Gallery.css'

function Gallery() {
  const pictures = [
    'club_alvaro_obregon/thumbnail_5_gzusgi.jpg*Álvaro Obregón',
    'club_amealco_de_hombres_mujeres/IMG_8089_ypf9jw.jpg*Amealco de Hombres & Mujeres',
    'club_apetlanca/IMG_8203_jux4f3.jpg*Apetlanca',
    'club_buenavista%20de%20cuellar/152776536_2942286112668771_8601712829451737993_n_qylu4e.jpg*Buenavista de Cuellar',
    'club_cieneguillas/152826847_135574718432176_3208876539763130664_n_py4tol.jpg*Cieneguillas',
    'club_el_fraile/IMG_8617_susftv.jpg*El Fraile',
    'club_el_paraiso/IMG_8073_e8fc25.jpg*El Paraíso',
    'club_el_potrero/IMG_8062_bclcvt.jpg*El Potrero',
    'club_huitzuco/aa9cb409-a55e-45b6-b950-bc7a6f7f2cce_vfhjdn.jpg*Huitzuco',
    'club_metlapa/IMG_8611_xxcolo.jpg*Metlapa',
    'club_nuevo_balsas/2846b958-4d1b-4bf3-acf5-00af323916b4_jag73n.jpg*Nuevo Balsas',
    'club_olinaltecos_en_accion/d61fb951-d33e-40b9-98fc-b143e0b1960b_ce5hu0.jpg*Olinaltecos en Acción',
    'club_pipincatla/IMG_8224_n5wymp.jpg*Pipincatla',
    'club_puente%20campuzano/thumbnail_1_tkdlbl.jpg*Puente Campuzano',
    'club_temaxcalapa/IMG_8083_kiwvow.jpg*Temaxcalapa',
    'club_teucizapan/420e8071-2294-4f95-a3d2-86b074d0a254_qxacnb.jpg*Teucizapan',
    'club_tijuanita/thumbnail_2_xwevpv.jpg*Tijuanita',
    'club_tlatzala/IMG_8117_orp9mp.jpg*Tlatzala',
    'club_tonalapa%20del%20sur/e38b0909-2f87-456d-8b9d-e9745febdce4_jgqbnz.jpg*Tonalapa del Sur',
    'club_xalostoc/IMG_8069_higxei.jpg*Xalostoc',
    'club_xonacatla/IMG_8181_iimwd2.jpg*Xonacatla'
  ]

  const HasName = (uri) => {
    return uri.includes("*");
  }

  const GetUri = (uri) => {
    return uri.split("*")[0]
  }

  const GetName = (uri) => {
    return uri.split("*")[1];
  }

  const GetClubUri = (uri) => {
    return uri.split("*")[1].replace("&", '').replace("  ", ' ').replace(/ /g, "_").toLowerCase();
  }

  return (
    <>
      <div id="clubs" className="twentyfv-height flexible full-width center columns bm">
        <h4 className="flexible center full-width primary uppercase tm2 bm">Clubes</h4>
        <h1 className="flexible center secondary text-center bm2">Conoce Nuestros Clubes</h1>
        <div className="flexible center rows g-c wrap bm2">
          {
            pictures.map(picture => {
              if (HasName(picture)) {
                return (
                  <NavLink to={"/clubs/"+GetClubUri(picture)} className="g-ca zoom popup">
                    <img src={`https://res.cloudinary.com/trinum-daniel/image/upload/w_500/federacion-de-guerrerenses/${GetUri(picture)}`} alt=""/>
                      {
                        HasName(picture) &&
                        <div className="img-label flexible center tertiary secondaryFill">Club {GetName(picture)}</div>
                      }
                  </NavLink>
                )
              }

              return (
                <div className="g-ca zoom popup">
                  <img src={`https://res.cloudinary.com/trinum-daniel/image/upload/w_500/federacion-de-guerrerenses/${GetUri(picture)}`} alt=""/>
                </div>
              )
            })
          }
        </div>
      </div>
    </>
  )
}

export default Gallery;
