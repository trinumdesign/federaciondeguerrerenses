import React from 'react';
import '../styles/Events.css'
import { NavLink } from 'react-router-dom';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';

function Events() {

  return (
    <>
      <div id="events" className="svnty-height flexible full-width center columns wrap">
        <h4 className="flexible center full-width primary uppercase tm2 bm2">Eventos</h4>
        <h1 className="flexible center secondary text-center uppercase bm">Lo Más Reciente</h1>
        <div className="flexible center rows wrap full-width c-cont bm2 tm">
          {/* <div className="flexible single rows popup clickable wrap">
            <div className="evimg zoom">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_1000/federacion-de-guerrerenses/portada_guerrero/foto_para_pagina_2021-01_x4zj1u.jpg" alt="event"/>
            </div>
            <div className="spacer flexible columns">
              <h5 className="opaque tertiary-dark">Febrero 21, 2021</h5>
              <br/>
              <h2 className="tertiary-dark">Semana Cultural Guerrerense 2021</h2>
              <br/>
              <p className="tertiary-dark">&#x25cf; <span className="bold uppercase">Mensaje de nuestro presidente...</span><br/>"Convertirnos en una organización de clubes de oriundos fuerte y sólida no ha sido fácil. Hemos recorrido un camino difícil para lograrlo. Gracias a todos los que nos han apoyado en estos años. <br/><br/>A pesar de vivir tiempos difíciles estamos perseverando y continuamos ayudando a nuestra gente y sus familias. Este 2021 celebramos 25 años de nuestra organización y la pandemia de COVID-19 nos ha obligado a hacerlo de manera virtual. Bienvenidos y disfruten de nuestra celebración".<br/><br/><span className="secondary">&#x25cf; Haga click para ver el mensaje completo en video...</span></p>
              <div className="spacer"></div>
              <img className="ev-fc" src="https://res.cloudinary.com/trinum-daniel/image/upload/v1613875691/federacion-de-guerrerenses/PHOTO-2021-02-20-20-45-06_n1iqyp.jpg" alt="face"/>
              <h5 className="tertiary-dark">Alfredo Arroyo</h5>
              <h5 className="tertiary-dark opaque">Presidente.</h5>
            </div>
            <NavLink to="/blog/02_21_2021/mensaje-de-presidente"></NavLink>
          </div> */}
          <div className="flexible pair l columns popup">
            <div className="evimg">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/Cqm-s9LKSXs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen"  msallowfullscreen="msallowfullscreen"  oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen">></iframe>
            </div>
            <div className="spacer flexible columns">
              <h5 className="opaque tertiary-dark">Febrero 21, 2021</h5>
              <br/>
              <h2 className="tertiary-dark">Video Conmemorativo "Semana Cultural Guerrerense 2021"</h2>
              <br/>
              <p className="tertiary-dark">&#x25cf; <span className="bold uppercase">Haga click para ver el Video Conmemorativo "Semana Cultural Guerrerense 2021"</span><br/><br/><span className="secondary">&#x25cf; Subscribete a nuestro canal de Youtube para ver mas sobre nuestro contenido!</span><br/><br/></p>
              <div className="spacer"></div>
              <h5 className="tertiary-dark">Federación de Guerrerenses</h5>
              <h5 className="tertiary-dark opaque">Chicago, Illinois</h5>
            </div>
          </div>
          <div className="flexible pair r columns popup clickable">
            <div className="evimg zoom">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_600/federacion-de-guerrerenses/portada_guerrero/foto_para_pagina_2021-01_x4zj1u.jpg" alt="event"/>
            </div>
            <div className="spacer flexible columns">
              <h5 className="opaque tertiary-dark">Febrero 21, 2021</h5>
              <br/>
              <h2 className="tertiary-dark">Semana Cultural Guerrerense 2021</h2>
              <br/>
              <p className="tertiary-dark">&#x25cf; <span className="bold uppercase">Mensaje de nuestro presidente...</span><br/>"Convertirnos en una organización de clubes de oriundos fuerte y sólida no ha sido fácil. Hemos recorrido un camino difícil para lograrlo. Gracias a todos los que nos han apoyado en estos años. "<br/><br/><span className="secondary">&#x25cf; Haga click para ver el mensaje completo en video...</span><br/><br/></p>
              <div className="spacer"></div>
              <h5 className="tertiary-dark">Federación de Guerrerenses</h5>
              <h5 className="tertiary-dark opaque">Chicago, Illinois</h5>
            </div>
            <NavLink to="/blog/02_21_2021/mensaje-de-presidente"></NavLink>
          </div>
          <div className="flexible pair l columns popup clickable">
            <div className="evimg zoom">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1613876102/federacion-de-guerrerenses/despensas_junio/IMG_8114_1_eabnwu.jpg" alt="event"/>
            </div>
            <div className="spacer flexible columns">
              <h5 className="opaque tertiary-dark">Junio, 2020</h5>
              <br/>
              <h2 className="tertiary-dark">Entrega de despensas para los afectados por la pandemia de COVID-19</h2>
              <br/>
              <p className="tertiary-dark opaque">Nuestras familias afectadas en pandemia no han sido olvidadas. Hemos llevado a cabo entrega de despensas para quienes han sufrido los efectos del COVID-19.</p>
              <div className="spacer"></div>
              <h5 className="tertiary-dark">Federación de Guerrerenses</h5>
              <h5 className="tertiary-dark opaque">Chicago, Illinois</h5>
            </div>
            <NavLink to="/blog/06_01_2020/entrega-de-despensas-covid19"></NavLink>
          </div>
          <div className="flexible pair r columns popup clickable">
            <div className="evimg zoom">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1613933398/federacion-de-guerrerenses/senorita_guerrero_2019/IMG_8301_tfa2ao.jpg" alt="event"/>
            </div>
            <div className="spacer flexible columns">
              <h5 className="opaque tertiary-dark">Julio, 2019</h5>
              <br/>
              <h2 className="tertiary-dark">Aprendiendo sobre nuestra cultura y tradiciones</h2>
              <br/>
              <p className="tertiary-dark opaque">Las participantes en el evento Señorita Guerrero en Chicago 2019, fueron invitadas a nuestro estado para conocer diferentes lugares y algunas de nuestras tradiciones.</p>
              <div className="spacer"></div>
              <h5 className="tertiary-dark">Federación de Guerrerenses</h5>
              <h5 className="tertiary-dark opaque">Chicago, Illinois</h5>
            </div>
            <NavLink to="/blog/07_01_2019/evento-senorita-guerrero"></NavLink>
          </div>
          <div className="flexible pair l columns popup clickable">
            <div className="evimg zoom">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1613940220/federacion-de-guerrerenses/portada_guerrero/16a49c2b-cba7-4d58-baa3-b51f14750658_mcr3kx.jpg" alt="event"/>
            </div>
            <div className="spacer flexible columns">
              <h5 className="opaque tertiary-dark">Febrero, 2021</h5>
              <br/>
              <h2 className="tertiary-dark">Homenaje a compañeros y amigos que ya no están entre nosotros</h2>
              <br/>
              <p className="tertiary-dark">&#x25cf; En el último año, no solo la pandemia de COVID-19 sino otras enfermedades o por causas naturales, algunos de nuestros compañeros de la organización, activistas comunitarios colegas y grandes colaboradores y amigos de nuestra causa proinmigrante han fallecido, dejando un enorme vacío entre nosotros.<br/><br/><span className="secondary">&#x25cf; Haga click para ver el obituario completo...</span></p>
              <div className="spacer"></div>
              <h5 className="tertiary-dark">Federación de Guerrerenses</h5>
              <h5 className="tertiary-dark opaque">Chicago, Illinois</h5>
            </div>
            <NavLink to="/blog/02_21_2021/homenaje-a-companeros-covid19"></NavLink>
          </div>
          <div className="flexible pair r columns popup clickable">
            <div className="evimg zoom">
              <img src="https://res.cloudinary.com/trinum-daniel/video/upload/c_crop,g_center,ar_1.33/v1613942101/federacion-de-guerrerenses/b89de36b-5559-44e2-aa9a-ff6e7ad7356e_rpekh8.png" alt="event"/>
            </div>
            <div className="spacer flexible columns">
              <h5 className="opaque tertiary-dark">Febrero, 2021</h5>
              <br/>
              <h2 className="tertiary-dark">ISAAC HELGUERA: Un artista ¡Orgullo de Guerrero!</h2>
              <br/>
              <p className="tertiary-dark"><span className="no">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam corporis qui officiis ad et esse officia nobis aut dicta quas mollitia, quis, distinctio voluptatem quibusdam temporibus minus delectus laborum natus sapiente, voluptate soluta maxime. Neque distinctio totam similique nam qui.</span><br/><br/><span className="secondary">&#x25cf; Haga click para ver el video completo...</span></p>
              <div className="spacer"></div>
              <h5 className="tertiary-dark">Federación de Guerrerenses</h5>
              <h5 className="tertiary-dark opaque">Chicago, Illinois</h5>
            </div>
            <NavLink to="/blog/02_21_2021/orgullo-de-guerrero"></NavLink>
          </div>
        </div>
        <br/>
        <br/>
      </div>
    </>
  )
}

export default Events;
