import React from 'react';
import '../styles/Blog.css'
import { NavLink } from 'react-router-dom';
import { ImFacebook, ImYoutube,  ImEarth } from 'react-icons/im';

import events from '../blogs/directory';

function Blog(props) {
  const [title, setTitle] = React.useState("");
  const [timestamp, setTimestamp] = React.useState("");
  const [socials, setSocials] = React.useState([]);
  const [sections, setSections] = React.useState([]);
  const [blogExist, setBlogExists] = React.useState(true);
  const [bigImage, setBigImage] = React.useState('');

  React.useEffect(() => {
    if (props.match.params.date in events) {
      let content;
      try {
        content = require('../blogs/'+props.match.params.identifier).default;
        setTitle(content.title);
        setTimestamp(content.timestamp);
        setSocials(content.socials);
        setSections(content.sections);
      } catch (e) {
        setBlogExists(false);
      }
    }
  }, [props.match.params])

  const GetSocialUri = s => {
    return s.split("*")[1]
  }

  return (
    <>
      <div id="blog" className="svnty-height flexible full-width columns wrap tm2">
        {
          !blogExist &&
          <h1 className="flexible center secondary text-center">El articulo que busca no existe</h1>
        }
        <h4 className="flexible center full-width primary bm">{timestamp}</h4>
        <h1 className="flexible center secondary text-center">{title}</h1>
        {
          socials.length > 0 &&
          <div className="flexible full-width center rows">
            {
              socials.map(social => {
                return (
                  <a href={GetSocialUri(social)} target="_blank" className="b-s flexible center popup clickable">
                    {
                      social.includes("fb*") &&
                      <ImFacebook></ImFacebook>
                    }
                    {
                      social.includes("yt*") &&
                      <ImYoutube></ImYoutube>
                    }
                    {
                      social.includes("web*") &&
                      <ImEarth></ImEarth>
                    }
                  </a>
                )
              })
            }
          </div>
        }
        <br/>
        <div className="half-width flexible rows wrap b-c">
          {
            sections.map(section => {
              if (section.type === 'uriximg') {
                return (
                  <div onClick={() => setBigImage(section.content)} className="b-c-i zoom clickable">
                    <img src={section.content} alt="snap" />
                  </div>
                )
              }

              if (section.type === 'urixvid') {
                return (
                  <div className="b-c-i">
                    <video controls>
                      Your browser does not support the video tag
                      <source src={section.content} type="video/mp4" />
                    </video>
                  </div>
                )
              }

              if (section.type === 'header') {
                return (
                  <h2 className="primary">{section.content}</h2>
                )
              }

              if (section.type === 'txtxlst') {
                return (
                  section.content.split(',').map(n => {
                    return (
                      <h4 className="tertiary-dark opaque l">&bull;{n}</h4>
                    )
                  })
                )
              }

              if (section.type === 'txtxbld') {
                return (
                  <h3 className="tertiary-dark">{section.content}</h3>
                )
              }

              return (
                <p className="tertiary-dark opaque">{section.content}</p>
              )
            })
          }
        </div>
        <br/>
        <br/>
      </div>
      <div onClick={() => setBigImage('')} className={"shadow " + (bigImage !== "" ? 'show' : '')}></div>
      <div className={"modal " + (bigImage !== "" ? 'show' : '')} >
        <img src={bigImage.split("w_300/").join('')}  alt=""/>
      </div>
    </>
  )
}

export default Blog;
