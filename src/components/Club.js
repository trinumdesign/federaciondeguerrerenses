import React from 'react';
import { NavLink } from 'react-router-dom';
import { FiChevronRight } from 'react-icons/fi';

import '../styles/Club.css'

function Club(props) {
  const [title, setTitle] = React.useState('');
  const [pictures, setPictures] = React.useState([]);
  const [bigImage, setBigImage] = React.useState('');
  const [note, setNote] = React.useState('');

  const clubs = {
    'álvaro_obregón': {
      name: 'Álvaro Obregón',
      note: "La colonia Álvaro Obregón se originó en 1935 con la llegada de familias provenientes del pueblo de Teucizapan, del Municipio Ixcateopan de Cuauhtémoc, quienes se vieron obligadas a recorrer nuevos rumbos por razones políticas, emigrando en busca de un mejor porvenir para los suyos. Actualmente esa comunidad cuenta con servicios públicos básicos como red de agua potable doméstica, drenaje, muchas calles pavimentadas e importantes obras realizadas gracias al Programa 3x1 Para Migrantes con el apoyo de los tres niveles del gobierno, la comunidad y los migrantes radicados en Estado Unidos, principalmente en la Ciudad de Chicago y en Santa Ana, California.",
      pictures: [
        'club_alvaro_obregon/thumbnail_3_wtci2t.jpg',
        'club_alvaro_obregon/thumbnail_5_gzusgi.jpg',
        'club_alvaro_obregon/thumbnail_1_f8nn5p.jpg',
        'club_alvaro_obregon/thumbnail_7_b3coxr.jpg',
        'club_alvaro_obregon/thumbnail_i2wrej.jpg'
      ]
    },
    'amealco_de_hombres_mujeres': {
      name: 'Amealco de Hombres & Mujeres',
      note: "El Comité Amealco de hombres nació en julio del 1989. “Nos llena de mucho orgullo decir que fuimos los pioneros. Los primeros mexicanos en Estados Unidos en organizarnos para ayudar a nuestras comunidades de origen en México”, dice Isidro Arroyo, uno de los fundadores. “Y también fuimos pioneros en el programa 3X1. Nos satisface demasiado que otras comunidades hayan seguido nuestros pasos”, puntualizó Arroyo. El Comité Amealco de mujeres inició actividades en mayo del año 2012. La primera presidenta de ese comité fue Cecilia Arroyo. Una de las obras prioritarias en la agenda de trabajo de estas damas voluntarias fue la construcción de las gradas en la plaza de toros y continuar con los tramos pendientes de la carretera Amealco -San Miguel. Para las mujeres de este club, el análisis siempre fue muy simple: “Si la gente nos está apoyando ¿Por qué no hacemos más bailes familiares?, decían en cada reunión. Así se hizo y gracias a ese buen consejo, se han logrado grandes obras para esa población guerrerense. pueblo.",
      pictures: [
        'club_amealco_de_hombres_mujeres/IMG_8089_ypf9jw.jpg',
        'club_amealco_de_hombres_mujeres/IMG_8090_sfajyb.jpg',
        'club_amealco_de_hombres_mujeres/IMG_8093_agyw78.jpg',
        'club_amealco_de_hombres_mujeres/IMG_8095_cy4jbh.jpg',
        'club_amealco_de_hombres_mujeres/IMG_8105_vvhmrz.jpg'
      ]
    },
    'apetlanca': {
      name: 'Apetlanca',
      note: "El Club Apetlanca es uno de los fundadores de la Federación de Guerrerenses de Illinois y por más de dos décadas, ha estado lealmente afiliado a esta organización. Con el apoyo de la actual representación de migrantes, dicen algunos de sus miembros, han evaluado en equipo la realización de proyectos que puedan ayudar a su comunidad de origen. Destaca en esta comunidad la enorme participación de sus jóvenes en cada una de la actividades cívicas y comunitarias que llevan a cabo de manera constante.",
      pictures: [
        'club_apetlanca/IMG_8203_jux4f3.jpg',
        'club_apetlanca/IMG_8201_evxqx8.jpg',
        'club_apetlanca/IMG_8200_ulu9bm.jpg',
        'club_apetlanca/IMG_8204_k8lgs1.jpg',
        'club_apetlanca/IMG_8202_eixvbd.jpg'
      ]
    },
    'buenavista_de_cuellar': {
      name: 'Buenavista De Cuellar',
      note: "Buenavista de Cuéllar es uno de los 81 municipios que conforman el estado de Guerrero. Forma parte de la región Norte del estado y su cabecera municipal es la ciudad que lleva su mismo nombre: “Buenavista de Cuéllar”. Limita al norte con el estado de Morelos, al sur con los municipios de Iguala y Huitzuco de los Figueroa, al este con el municipio de Huitzuco de los Figueroa y el estado de Morelos y al oeste con el municipio de Taxco de Alarcón.  El municipio de Buenavista de Cuéllar está integrado por 38 localidades. En 1939 formó parte del municipio de Iguala, y fue apenas el 13 de diciembre de 1944 cuando se constituye Buenavista de Cuéllar como un nuevo municipio en el Estado de Guerrero.",
      pictures: [
        'club_buenavista%20de%20cuellar/152776536_2942286112668771_8601712829451737993_n_qylu4e.jpg',
        'club_buenavista%20de%20cuellar/152826847_135574718432176_3208876539763130664_n_falgpq.jpg',
        'club_buenavista%20de%20cuellar/152446878_340948493850826_6432402210784351501_n_p1kq7p.jpg',
        'club_buenavista%20de%20cuellar/151582564_191887022714598_928266955053091036_n_lzy542.jpg'
      ]
    },
    'cieneguillas': {
      name: 'Cieneguillas',
      note: "Enclavado en las montañas se encuentra Cieneguillas, una comunidad perteneciente al Municipio de Buenavista de Cuéllar. Es como un “nido de alegres montañas” que limita con la ciudad de Iguala y la comunidad de El Naranjo. Cuenta con aproximadamente 200 habitantes y está a 1,080 metros de altitud sobre el nivel del mar.  El 3,61% de la población es indígena, y el 3,09% de los habitantes habla una lengua indígena. El 35,57% de la población mayor de 12 años está ocupada laboralmente (el 62,64% de los hombres y el 11,65% de las mujeres).",
      pictures: [
        'club_cieneguillas/152826847_135574718432176_3208876539763130664_n_py4tol.jpg',
        'club_cieneguillas/152446878_340948493850826_6432402210784351501_n_i7n5am.jpg',
        'club_cieneguillas/152776547_2884612981817640_987410289323711164_n_fwkawk.jpg',
        'club_cieneguillas/152776536_2942286112668771_8601712829451737993_n_ohnc43.jpg',
        'club_cieneguillas/151582564_191887022714598_928266955053091036_n_otswyi.jpg',
        'club_cieneguillas/152776547_2884612981817640_987410289323711164_n_x3ym1k.jpg',
        'club_cieneguillas/152883788_474750960227626_6848700867640593748_n_ju4dv7.jpg',
        'club_cieneguillas/IMG_8152_mv9mfu.jpg'
      ]
    },
    'el_fraile': {
      name: 'El Fraile',
      note: "El pintoresco pueblo de El Fraile hace parte del Municipio de Taxco de Alarcón. Tiene aproximadamente 400 habitantes y se localiza a 1,490 metros de altitud sobre el nivel del mar. El Fraile ha sido durante muchos años una comunidad minera por excelencia, aunque en la actualidad solo una mina está activa y en explotación.",
      pictures: [
        'club_el_fraile/IMG_8617_susftv.jpg',
        'club_el_fraile/IMG_8616_q9krza.jpg',
        'club_el_fraile/IMG_8618_sgkpfd.jpg',
        'club_el_fraile/IMG_8619_vh3bgq.jpg',
        'club_el_fraile/IMG_8623_wc8ibh.jpg'
      ]
    },
    'el_paraiso': {
      name: 'El Paraiso',
      note: "El Paraíso está situado en el Municipio de Atoyac de Álvarez. En la localidad hay poco más de 4,000 habitantes y su altitud sobre el nivel del mar es de 799 metros. Los pobladores se dividen en 2,084 menores de edad y 2,021 adultos, de cuales 303 tienen más de 60 años. 194 personas en El Paraíso viven en hogares indígenas.  Derecho a atención médica por el Seguro Social tienen 156 habitantes, de un total de 839 hogares. De estos, 332 tienen piso de tierra y 66 cuentan con una sola habitación. 693 de todas las viviendas tienen instalaciones sanitarias, 349 son conectadas al servicio público y 809 tienen acceso a la luz eléctrica. La estructura económica permite a 34 viviendas tener una computadora, a 166 tener una lavadora y 650 tienen acceso a televisión.",
      pictures: [
        'club_el_paraiso/IMG_8073_e8fc25.jpg',
        'club_el_paraiso/20a6cd9f-7147-4e06-82ac-aa2c8cf0cd11_lpw4kg.jpg',
        'club_el_paraiso/IMG_8067_b1op0n.jpg',
        'club_el_paraiso/IMG_8075_iitxvq.jpg'
      ]
    },
    'el_potrero': {
      name: 'El Potrero',
      note: "Estar en El Potrero es estar en casa. Es llegar y ver a toda tu familia reunida. Eso es El Potrero, ¡Un pedacito de cielo en la tierra! Allí están los recuerdos más bellos en la memoria de cada uno de sus habitantes. Recuerdos que se llevan por siempre. Allá reposan lindos recuerdos de los antepasados, que sin duda jamás se irán. Y por más lejos que se encuentren los hijos de esa tierra, siempre regresarán a buscar ese sentimiento, metido muy adentro de sus venas.",
      pictures: [
        'club_el_potrero/IMG_8058_e54dqx.jpg',
        'club_el_potrero/IMG_8062_bclcvt.jpg',
        'club_el_potrero/IMG_8060_pap1jf.jpg',
        'club_el_potrero/IMG_8062_bclcvt.jpg',
        'club_el_potrero/IMG_8063_jsl047.jpg'
      ]
    },
    'huitzuco': {
      name: 'Huitzuco',
      note: "Huitzuco se constituyó en 1850 y fue uno de los 38 municipios existentes al erigirse el Estado de Guerrero. Existen diferentes versiones sobre el significado de la palabra “Huitzuco”. Algunos afirman que proviene de los vocablos náhuatl Huitzilizo: “Espinas”, y el locativo co, por lo que en conjunto se interpreta  como: \"Cosa que contiene espinas\", debido a que sus terrenos se hallaban cubiertos de huizache.",
      pictures: [
        'club_huitzuco/352fa3d9-055c-4a82-96ea-de9e64aacadc_pretkl.jpg',
        'club_huitzuco/aa9cb409-a55e-45b6-b950-bc7a6f7f2cce_vfhjdn.jpg',
        'club_huitzuco/c307ade8-f8aa-46a7-9884-03c5f8d93a75_kbavpu.jpg',
        'club_huitzuco/d4478bc2-c420-4b21-8d4d-8554ae7d4500_vseure.jpg'
      ]
    },
    'metlapa': {
      name: 'Metlapa',
      note: "La comunidad de Metlapa se localiza en el Municipio Iguala de la Independencia. La población total es de 1,400 personas aproximadamente. Los ciudadanos se dividen en 560 menores de edad y 818 adultos, de cuales 154 tienen más de 60 años.10 personas en Metlapa viven en hogares indígenas. Derecho a atención médica por el Seguro Social tienen 110 de sus habitantes y en la población hay un total de 328 hogares.",
      pictures: [
        'club_metlapa/IMG_8611_xxcolo.jpg',
        'club_metlapa/IMG_8608_vejpff.jpg',
        'club_metlapa/IMG_8609_e6dlog.jpg',
        'club_metlapa/IMG_8614_bvi1we.jpg',
        'club_metlapa/IMG_8615_hucje3.jpg'
      ]
    },
    'nuevo_balsas': {
      name: 'Nuevo Balsas',
      note: "La localidad de Nuevo Balsas está situada en el Municipio de Cocula a 490 metros de altitud sobre el nivel del mar. Cuenta con aproximadamente 1,700 habitantes y en la lista de los pueblos más poblados de todo el municipio, es el número 3 del ránking. En la localidad hay 847 hombres y 864 mujeres. Del total de la población, el 4.15% proviene de fuera del Estado de Guerrero. El 15.84% de la población es analfabeta (el 15.11% de los hombres y el 16.55% de las mujeres). El grado de escolaridad es del 5.35 (5.41 en hombres y 5.29 en mujeres). El 0.41% de la población es indígena y el 0.23% de los habitantes habla una lengua indígena. El 31.27% de la población mayor de 12 años está ocupada laboralmente (el 51.24% de los hombres y el 11.69% de las mujeres).\nEn Nuevo Balsas hay 681 viviendas.",
      pictures: [
        'club_nuevo_balsas/2846b958-4d1b-4bf3-acf5-00af323916b4_jag73n.jpg',
        'club_nuevo_balsas/1e7f638f-a1bb-40ed-9ebb-a3430cf80ca0_rsru4d.jpg',
        'club_nuevo_balsas/4f7cfcbb-51d2-4974-a51a-c0e34f59c147_bmvvzl.jpg',
        'club_nuevo_balsas/cbe403f7-2b69-4999-91db-de743016dd26_qpynbj.jpg',
        'club_nuevo_balsas/fada3a11-d363-4087-86e7-2218d0240c77_ddd1aa.jpg'
      ]
    },
    'olinaltecos_en_acción': {
      name: 'Olinaltecos En Accion',
      note: "Si de algo están orgullosos los integrantes del Club Olinaltecos en Acción es de conservar la unidad y la esperanza como migrantes, y sobre todo, seguir aportando un granito de arena en apoyo a su comunidad en diferentes actividades, como por ejemplo: promover la buena salud impulsando la práctica deportiva entre los jóvenes, apoyando a los artesanos y ayudando a hacer realidad el programa “Uniendo Corazones” para que las personas mayores de 62 años puedan reunirse con sus familias en Estados Unidos, después de muchos años de ausencia. El comité cuenta con destacados reconocimientos como el otorgado hace algunos años por el Consulado de México en Chicago en las actividades del evento “Celebrando México en el Navy Pier”. También fueron galardonados por la Sociedad Cívica Mexicana de Illinois con el máximo reconocimiento que otorga esa organización cada año, al club comunitario mexicano más sobresaliente.",
      pictures: [
        'club_olinaltecos_en_accion/8dce1af9-25c1-4fa5-b111-0418361888c1_hfwe86.jpg',
        'club_olinaltecos_en_accion/d61fb951-d33e-40b9-98fc-b143e0b1960b_ce5hu0.jpg',
        'club_olinaltecos_en_accion/59c9400c-9ce6-4f26-bb26-f493247986e4_imdgy0.jpg',
        'club_olinaltecos_en_accion/9e2801c7-ec9b-4853-bbbb-de01942a6d71_qz9txv.jpg',
        'club_olinaltecos_en_accion/daeb4213-724c-4f4c-8486-9d534b02298f_clqfa9.jpg'
      ]
    },
    'pipincatla': {
      name: 'Pipincatla',
      note: "La localidad de Pipincatla está situada en el Municipio de Ixcateopan de Cuauhtémoc y cuenta con aproximadamente 400 habitantes. Es el pueblo más poblado en la posición número 5 de todo el municipio. Pipincatla está a 1446 metros de altitud sobre el nivel del mar. Del total de la población, el 3.48% proviene de fuera del Estado de Guerrero. El 5.61% de la población es analfabeta (el 4.17% de los hombres y el 7.14% de las mujeres). El grado de escolaridad es del 6.53 (6.83 en hombres y 6.24 en mujeres). En Pipincatla hay un total de 74 hogares. De éstas 74 viviendas, 50 tienen instalaciones sanitarias, 48 son conectadas al servicio público y 73 tienen acceso a la luz eléctrica. La estructura económica permite a 4 viviendas tener una computadora, a 24 tener una lavadora y 67 que tienen un televisor.",
      pictures: [
        'club_pipincatla/IMG_8224_n5wymp.jpg',
        'club_pipincatla/IMG_8223_cuf1tq.jpg',
        'club_pipincatla/IMG_8221_khsy9z.jpg',
        'club_pipincatla/IMG_8220_gysgla.jpg'
      ]
    },
    'puente_campuzano': {
      name: 'Puente Campuzano',
      note: "Puente Campuzano se localiza en el Municipio Taxco de Alarcón. “Una de las obras más destacadas que hemos llevado a cabo en nuestra comunidad de origen, gracias al Programa 3x1 Para Migrantes, ha sido la calle principal y se preparan ya otros proyectos en beneficio de nuestro pueblo. Reconozco que es un poco difícil hoy en día, pero ahí la llevamos, con el apoyo del Gobierno a nivel Estatal y Municipal”, dice Miguel Martínez, en representación de este comité de oriundos.  Otras de las obras importantes que se han llevado a cabo en beneficio de esa comunidad han sido: el techado de la cancha de usos múltiples, con inversiones de los propios migrantes y el apoyo de dineros públicos del Gobierno del Estado y Municipal. También se llevó a cabo la ampliación y pavimentación de la calle de acceso al pueblo, con inversión de los migrantes.",
      pictures: [
        'club_puente%20campuzano/thumbnail_1_tkdlbl.jpg',
        'club_puente%20campuzano/thumbnail_6_ebjoit.jpg',
        'club_puente%20campuzano/thumbnail_ktyjkx.jpg',
        'club_puente%20campuzano/thumbnail_5_vcppka.jpg',
        'club_puente%20campuzano/thumbnail_3_zsv0f3.jpg'
      ]
    },
    'temaxcalapa': {
      name: 'Temaxcalapa',
      note: "El club temaxcalapense radicado en Chicago está afiliado a la Federación de Guerrerenses y representa con mucho orgullo a su pueblo TEMAXCALAPA, en el Municipio de Taxco de Alarcón.  \"Gracias al Programa 3x1 nuestra comunidad se ha beneficiado en gran manera con obras de infraestructura, como por ejemplo una carretera de 12 kilómetros de longitud por 5 metros de amplitud y 15 centímetros de espesor. Otro proyecto ya realizado fue la pavimentación rumbo al Panteón Municipal, conocido como \"La subida del Colorín\" en el Barrio Oriente\", manifiesta Juvenal Torres en representación de este comité. \"Finalmente deseo dar las gracias a Primera Dama de Guerrero, Sra. Mercedes Calvo de Astudillo, por la implementación del Programa Uniendo Corazones, que sin duda ha dado gran alegría a varias de nuestras familias que se han vuelto a reunir después de muchos años. Invitamos a otras comunidades para que se afilien a la Federación de Guerrerenses y de esta manera puedan ayudar a sus comunidades de origen, allá en México\", puntualizó el activista comunitario.",
      pictures: [
        'club_temaxcalapa/IMG_8083_kiwvow.jpg',
        'club_temaxcalapa/IMG_8084_hoky1m.jpg',
        'club_temaxcalapa/IMG_8087_di6dae.jpg',
        'club_temaxcalapa/IMG_8086_a5dmfg.jpg',
        'club_temaxcalapa/IMG_8085_wrffro.jpg'
      ]
    },
    'teucizapan': {
      name: 'Teucizapan',
      note: "La palabra Teucizapan significa en Náhuatl: \"Río Subterráneo\". Esta comunidad fue fundada en 1668 por pobladores de Ixcateopan de Cuauhtémoc que buscaban más tierras para sus cultivos y actividades de ganadería. Aunque la verdad, el significado de la palabra Teucizapan que más se adapta a la actualidad es el de: “Tierra de mujeres hermosas” y no es exageración. La belleza de sus mujeres ha quedado demostrada con numerosos triunfos en anteriores certámenes de elección y coronación de la Reina Guerrero en Chicago organizada cada año en el mes de febrero por la Federación de Guerrerenses.",
      pictures: [
        'club_teucizapan/420e8071-2294-4f95-a3d2-86b074d0a254_qxacnb.jpg',
        'club_teucizapan/IMG_8193_lsrmrm.jpg',
        'club_teucizapan/ec196305-dd60-41e0-aad4-6b1c76beda2a_omvbwu.jpg',
        'club_teucizapan/5e4c296e-b170-41cc-bf86-78733130d60f_ifozwd.jpg',
        'club_teucizapan/0b01990d-728e-4291-8b24-f6b9677de774_i72po0.jpg'
      ]
    },
    'tijuanita': {
      name: 'Tijuanita',
      note: "La localidad de Tijuanita hace parte del Municipio de Cocula y tiene aproximadamente 940 habitantes. Tijuanita está ubicado a 656 metros de altitud sobre el nivel del mar y cuenta 400 hombres y 540 mujeres. Del total de la población, el 3,86% proviene de fuera del Estado de Guerrero. Cada 25 de abril, el pueblo celebra su aniversario con un colorido desfile que cuenta con la participación de la mayoría de sus habitantes. Entre música y alegría, la gente disfruta de eventos culturales, bailes y jaripeos y se da la bienvenida a vecinos y amigos de pueblos cercanos. El Club Tijuanita está afiliado a la Federación de Guerrerenses de Illinois y fue presidido durante muchos años por el activista comunitario Silvano Delgado, fallecido no hace mucho en Chicago y recordado con gran aprecio por parte de la comunidad migrante no solo de la ‘ciudad de los vientos’, sino de todo el Estado de Illinois.",
      pictures: [
        'club_tijuanita/thumbnail_2_xwevpv.jpg',
        'club_tijuanita/thumbnail_vjwtzy.jpg',
        'club_tijuanita/thumbnail_4_f4z5zm.jpg',
        'club_tijuanita/thumbnail_3_wiuwea.jpg',
        'club_tijuanita/thumbnail_1_ru5s1t.jpg'
      ]
    },
    'tlatzala': {
      name: 'Tlatzala',
      note: "Etelverto Bustamante es la cabeza visible del Club Tlatzala afiliado a la Federación de Guerrerenses de Illinois. “A nombre de nuestro comité en Chicago, quiero dar las gracias a todos los paisanos y amigos que han cooperado para que las obras de enbellecimiento e infraestructura en nuestro pueblo hayan sido hoy en día una realidad”, manifiesta el destacado líder comunitario. Don Etelverto, como también le dicen muchos de sus paisanos en Chicago, recordó que “cada mes de enero, el club envía recursos para comprarle Rosca de Reyes y juguetes a todos los niños de Tlatzala”.  Es importante mencionar que el Club Tlatzala Chicago también donó en años anteriores un equipo de sonido a la escuela primaria Josefa Ortíz de Domínguez, entregaron tres toneladas de cemento a la telesecundaria Emiliano Zapata, además de uniformes de baloncesto para los equipos femenil y varonil de esa misma institución educativa. “Otra donación que logramos hacer fue para el kinder Leona Vicario. En esa ocasión, gracias al apoyo de todos, pudimos entregar recursos económicos para terminar de construir los baños para los chiquitines de ese centro educativo”, dijo finalmente Etelverto Bustamente.",
      pictures: [
        'club_tlatzala/IMG_8117_orp9mp.jpg',
        'club_tlatzala/IMG_8120_ucmmk2.jpg',
        'club_tlatzala/IMG_8116_xd0k3r.jpg',
        'club_tlatzala/IMG_8119_ubsoco.jpg',
        'club_tlatzala/IMG_8115_phxi11.jpg'
      ]
    },
    'tonalapa_del_sur': {
      name: 'Tonalapa Del Sur',
      note: "Tonalapa del Sur es un pueblo que cultiva la tierra y donde manos laboriosas trabajan bellas artesanías de madera. Esta es una de las comunidades guerrerenses más beneficiadas por las bondades del Programa 3x1 Para Migrantes. Sus habitantes gozan actualmente de la remodelación del zócalo de la población, una obra que pudo lograrse gracias a los migrantes que residen en Chicago, conjuntamente con la Federación de Guerrerenses en Illinois y los fondos otorgados por el Programa 3X1 para Migrantes. El Club Tonalapa del Sur inició labores el 20 de enero de 2016 y desde esa fecha ha venido trabajando constantemente para lograr los objetivos y metas propuestas bajo el eslogan de: ¡Un pueblo unido, jamás será vencido!",
      pictures: [
        'club_tonalapa%20del%20sur/e38b0909-2f87-456d-8b9d-e9745febdce4_jgqbnz.jpg',
        'club_tonalapa%20del%20sur/aaae7e77-0b6d-4500-b9ab-7fb3c79fc2fa_ndzefh.jpg',
        'club_tonalapa%20del%20sur/IMG_8206_l5diev.jpg',
        'club_tonalapa%20del%20sur/IMG_8207_g5kypi.jpg',
        'club_tonalapa%20del%20sur/IMG_8205_kkcaau.jpg'
      ]
    },
    'xalostoc': {
      name: 'Xalostoc',
      note: "La comunidad de Xalostoc está situada en el Municipio de Ixcateopan de Cuauhtémoc. Dentro de todos los pueblos del municipio, ocupa el número 9 en cuanto a número de habitantes. Xalostoc está a 1426 metros de altitud sobre el nivel del mar. El pueblo de Xalostoc está situado a 10.4 kilómetros de Ixcateopan de Cuauhtémoc, que es la localidad más poblada del municipio, en dirección Noreste. La población total de Xalostoc es de 154 personas, de cuales 70 son masculinos y 84 femeninas. Los ciudadanos se dividen en 50 menores de edad y 104 adultos, de cuales 50 tienen más de 60 años.  En Xalostoc hay un total de 49 hogares.",
      pictures: [
        'club_xalostoc/IMG_8069_higxei.jpg',
        'club_xalostoc/IMG_8071_kjankl.jpg',
        'club_xalostoc/IMG_8082_z75pxm.jpg',
        'club_xalostoc/IMG_8080_amtwz3.jpg',
        'club_xalostoc/IMG_8068_bpcie2.jpg'
      ]
    },
    'xonacatla': {
      name: 'Xonacatla',
      note: "Xonacatla es una comunidad arraigada, donde el respeto por las costumbres y tradiciones son una prioridad. Habitada por gente sincera, amable y siempre dispuesta a recibir a los visitantes con las manos abiertas, ha hecho que sus Fiestas Patronales generen la visita de propios y extraños cada año. Esas tradicionales celebraciones se realizan cada 15 de febrero, con la asistencia de familias enteras de pueblos vecinos, inclusive de personas que viven en el exterior. El Club Xonacatla lleva 20 años asociado a la Federación de Guerrerenses. Durante su existencia se han realizado obras de gran valor económico para esa comunidad, entre ellas la pavimentación de carreteras con una inversión aproximada de 80,000 dólares. El comité cambia de mesa directiva cada dos años y de esta manera, prolonga su existencia dándole la oportunidad a caras nuevas para que adquieran mayor experiencia en las labores propias del activismo comunitario.",
      pictures: [
        'club_xonacatla/IMG_8181_iimwd2.jpg',
        'club_xonacatla/IMG_8180_zxkwcg.jpg',
        'club_xonacatla/IMG_8179_wxmkgg.jpg',
        'club_xonacatla/IMG_8178_cmhafv.jpg',
        'club_xonacatla/IMG_8176_mb3l1i.jpg'
      ]
    }
  }

  React.useEffect(() => {
    if (props.match.params.identifier in clubs) {
      setTitle(clubs[props.match.params.identifier].name);
      setPictures(clubs[props.match.params.identifier].pictures);
      setNote(clubs[props.match.params.identifier].note)
    } else {
      setTitle('El club que buscas no existe');
      setPictures([]);
      setNote('')
    }
  }, [props.match.params])

  const GetNextClub = () => {
    let keys = Object.keys(clubs).sort();
    let ci = 0;
    for (let i = 0; i < keys.length; i++) {
      console.log(props.match.params.identifier);
      if (keys[i] == props.match.params.identifier) {
        ci = i;
        break;
      }
    }
    if (ci < keys.length - 1) {
      return keys[ci + 1];
    } else {
      return keys[0]
    }
  }

  return (
    <>
      <div id="club" className="twentyfv-height flexible full-width center columns tm2">
        <h4 className="flexible center full-width primary bm">Nuestro Club</h4>
        <h1 className="flexible center secondary text-center bm2">{title}</h1>
        <p className="tertiary-dark opaque c-n bm2">&#x25cf; {note}</p>
        <div className="flexible center rows g-c wrap tm bm2">
          {
            pictures.map(picture => {
              return (
                <div onClick={() => setBigImage(picture)} className="g-ca zoom popup clickable">
                  <img src={`https://res.cloudinary.com/trinum-daniel/image/upload/w_500/federacion-de-guerrerenses/${picture}`} alt=""/>
                </div>
              )
            })
          }
        </div>
      </div>
      <div onClick={() => setBigImage('')} className={"shadow " + (bigImage !== "" ? 'show' : '')}></div>
      <div className={"modal " + (bigImage !== "" ? 'show' : '')} >
        <img src={`https://res.cloudinary.com/trinum-daniel/image/upload/federacion-de-guerrerenses/${bigImage}`}  alt=""/>
      </div>
      <NavLink to={`/clubs/${GetNextClub()}`} className="flexible center full-width primary bold n-c">Siguente Club <FiChevronRight/></NavLink>
      <br/>
      <br/>
    </>
  )
}

export default Club;
