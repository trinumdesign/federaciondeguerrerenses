import React from 'react';
import { FaFacebookF, FaInstagram, FaBehance, FaEtsy } from 'react-icons/fa'
import { AiFillMessage } from 'react-icons/ai'
import { FiMap } from 'react-icons/fi'
import { IoCall, IoMail } from 'react-icons/io5'
import { NavLink } from 'react-router-dom';

import '../styles/Footer.css'

function Footer() {
  return (
    <>
      <section id="footer" className="footer full-width flexible center rows wrap">
        <div className="flexible columns toe">
          <h2 className="primary">Sobre</h2>
          <br/>
          <p className="primary opaque">La Federación de Guerrerenses es una organización comunitaria de clubes de oriundos, sin fines de lucro, que tiene como objetivo primordial: “Enpoderar a la comunidad guerrerense radicada en el Medio Oeste de los Estados Unidos de America”.</p>
        </div>
        <div className="flexible columns toe">
          <h2 className="primary">Redes sociales</h2>
          <br/>
          <a href="https://www.facebook.com/FederacionDeGuerrerensesEnChicago" target="_blank" className="primary flexible rows"><FaFacebookF /><span className="opaque">Facebook</span></a>
          <p href="https://www.facebook.com/FederacionDeGuerrerensesEnChicago" className="primary flexible rows no"><FaFacebookF /><span className="opaque">Facebook</span></p>
          <p href="https://www.facebook.com/FederacionDeGuerrerensesEnChicago" className="primary flexible rows no"><FaFacebookF /><span className="opaque">Facebook</span></p>
        </div>
        <div className="flexible columns toe">
          <h2 className="primary">Contactanos</h2>
          <br/>
          <a href="tel:7739699744" className="primary flexible rows"><IoCall/><span className="opaque">(773) 969-9744</span></a>
          <a href="mailto:federacionguerrerochicago@gmail.com" className="primary flexible rows"><IoMail/><span className="opaque">federacionguerrerochicago@gmail.com</span></a>
          <p className="primary flexible rows"><FiMap/><span className="opaque">5120 W North Ave Chicago, IL 60639</span></p>
        </div>
        <h5 className="copyright-creds full-width center flexible">© 2021 Federacion Guerrerense en Chicago. All Rights Reserved. Developed by<a className="creds-ref" href="https://www.trinumdesign.com">&nbsp;Trinum Design Inc.</a></h5>
      </section>
    </>
  )
}

export default Footer;
