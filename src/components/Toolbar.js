import React from 'react';
import '../styles/Toolbar.css'
import { NavLink } from 'react-router-dom';

function Toolbar({show}) {
  return (
    <>
      <nav id="top" className={"flexible center elevate wrap primaryFill full-width"}>
        <div className="spacer"></div>
        <NavLink to="/events"  activeClassName="active" className="nav-tab hoverable tertiary bold flexible center uppercase">Eventos</NavLink>
        <NavLink to="/blogs"  activeClassName="active" className="nav-tab hoverable tertiary bold flexible center uppercase">Calendario</NavLink>
        <NavLink to="/">
          <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_256/federacion-de-guerrerenses/portada_guerrero/logo_1_y97fx1.png" alt="iconion" className="iconion"/>
        </NavLink>
        <NavLink to="/clubs"  activeClassName="active" className="nav-tab hoverable tertiary bold flexible center uppercase">Clubes</NavLink>
        <NavLink to="/contact"  activeClassName="active" className="nav-tab hoverable tertiary bold flexible center uppercase">Contacto</NavLink>
        <div className="spacer"></div>
      </nav>
    </>
  )
}

export default Toolbar;
